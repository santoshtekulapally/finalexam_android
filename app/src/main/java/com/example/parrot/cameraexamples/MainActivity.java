package com.example.parrot.cameraexamples;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "JENELLE";
    boolean lightOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

    }


    public void photoButtonPressed(View v) {

        // When person pushes the button, you need to get the photo from the photo gallery.
        // BUT -- you can only access the gallery if you have permission!!!!
        // So, when person presses button:
        // 1. ask for permission FIRST
        // 2. THEN go and get the photo from the gallery.


        // Check for permissions
        if (Build.VERSION.SDK_INT < 23) {
            // Person is using an old version of Android. Y
            // Just go to the photo gallery.
            getPhotoFromGallery();
        }
        else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // Person did NOT give permission, so start the process of asking for permission

                // You can choose ANY request code
                // It's just a random number that YOU choose
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);

            } else {
                // Person previously gave permission to your app, so
                // directly go to the gallery
                getPhotoFromGallery();

            }
        }

    }



    public void getPhotoFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // Remember: Choose any number for the request code!
        startActivityForResult(i, 123);
    }

    public void sendButtonPressed(View v) {

   showtorch();

    }


    public void showtorch() {
       // Toast.makeText(this, "PHONE IS SHAKING!!!!", Toast.LENGTH_SHORT).show();

























        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        try {
            String cameraId = cameraManager.getCameraIdList()[0];
            cameraManager.setTorchMode(cameraId, true);
            lightOn = true;
           // imageFlashlight.setImageResource(R.drawable.btn_switch_on);
        } catch (CameraAccessException e)
        {
                        Toast.makeText(this, "Error while turning on camera flash", Toast.LENGTH_SHORT).show();

        }


        // turn on flashlight when phone shakes!
//        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
//
//        // TURN ON THE FLASH on your phone!
//
//        try {
//            // 1. which camera do you want (Front or back camera?!)
//            String cameraID = cameraManager.getCameraIdList()[0];   // BACK CAMERA?
//
//            // 2. turn the flash on
//            // turn on
//
//            if (lightOn == false) {
//                cameraManager.setTorchMode(cameraID, true);
//                Log.d("JENELLE", "Turning flash ON!");
//                Toast.makeText(this, "Turning flash ON!", Toast.LENGTH_SHORT).show();
//
//                lightOn = true;
//            }
//            else {
//                cameraManager.setTorchMode(cameraID, false);
//
//                Log.d("JENELLE", "Turning flash OFF!");
//                Toast.makeText(this, "Turning flash OFF!", Toast.LENGTH_SHORT).show();
//
//                lightOn = false;
//            }
//
//            // turn off
//            //cameraManager.setTorchMode(cameraID, false);
//        }
//        catch (Exception e) {
//            Log.d("JENELLE", "Error while turning on camera flash");
//            Toast.makeText(this, "Error while turning on camera flash", Toast.LENGTH_SHORT).show();
//        }
    }

    // Function for showing the permission popup box.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // This request code should match the number you chose in the .requestPermissions(...) function.
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhotoFromGallery();
            }
        }

    }


    // After the user selects a photo, run this code
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // This function will run anytime an Activity closes.
        // The only way for your code to tell the difference between the Activities is
        // through the REQUESTCODE.  Every Activity should have a different code!
        if (requestCode == 123 && resultCode == RESULT_OK && data != null) {

            Uri imageFromGallery = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageFromGallery);
                ImageView imageView = (ImageView) findViewById(R.id.thePhoto);
                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
